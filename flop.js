(function($) {
  var click = false; // Allow Submit/Edit button
  var edit = false; // Dirty form flag
  
  Drupal.behaviors.flop = {
    attach: function (context, settings) {
      $('form.flop-enabled', context).once('flop', function() {
        var form = this;
        
        // If they leave an input or textarea field, assume they changed it.
        $(':input, textarea', form).each(function() {
          $(this).blur(function() {
            edit = true;
          });
        });
        
        $('select', form).each(function() {
          $(this).change(function() {
            edit = true;
          });
        });

        // Let all form submit buttons through
        $('input[type="submit"]', form).each(function() {
          $(this).addClass('flop-processed');
          $(this).click(function() {
            click = true;
          });
        });

        // Catch all links and buttons EXCEPT for "#" links
        $('a, button, input[type="submit"]:not(.flop-processed)')
        .each(function() {
          $(this).click(function() {
            // return when a "#" link is clicked so as to skip the
            // window.onbeforeunload function
            if (edit && $(this).attr("href") != "#") {
              return 0;
            }
          });
        });
      });
      
      // Handle backbutton, exit etc.
      window.onbeforeunload = function() {
        // Add CKEditor support
        if (typeof (CKEDITOR) != 'undefined' && typeof (CKEDITOR.instances) != 'undefined') {
          for ( var i in CKEDITOR.instances) {
            if (CKEDITOR.instances[i].checkDirty()) {
              edit = true;
              break;
            }
          }
        }
        if (edit && !click) {
          click = false;
          return (Drupal.t("You will lose all unsaved work."));
        }
      };
    }
  };
})(jQuery);