<?php

/**
 * @file
 * Admin page callback for the FLOP module.
 */

/**
 * Form for configuring Form Loss Protection
 */
function flop_page_admin($form, &$form_state) {
  
  $target_options = array(
    FLOP_FORMS_NOTLISTED => t('All forms except those listed'),
    FLOP_FORMS_LISTED => t('Only the listed forms'),
  );
  
  $form['flop_target_type'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Form Loss Protection on specific forms'),
    '#options' => $target_options,
    '#default_value' => variable_get('flop_target_type', FLOP_FORMS_NOTLISTED),
  );
  
  $form['flop_forms_id'] = array(
    '#type' => 'textarea',
    '#title' => '<span class="element-invisible">' . t('Forms') . '</span>',
    '#description' => 'Specify forms by using their form ID. Enter one form ID per line. The \'*\' character is a wildcard.',
    '#default_value' => variable_get('flop_forms', ''),
  );
  
  return system_settings_form($form);
}